package me.expdev.l00tinghome.util;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Project created by ExpDev
 */
public final class TextUtil {

    public static final transient Pattern patternTag = Pattern.compile("<([a-zA-Z0-9_]*)>");

    public static String parseColor(String string) {
        string = parseColorAmp(string);
        string = parseColorAcc(string);
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    // -------------------------------------------- //
    // Top-level parsing functions.
    // -------------------------------------------- //

    public static String parseColorAmp(String string) {
        string = string.replaceAll("(§([a-z0-9]))", "\u00A7$2");
        string = string.replaceAll("(&([a-z0-9]))", "\u00A7$2");
        string = string.replace("&&", "&");
        return string;
    }

    public static String parseColorAcc(String string) {
        return string.replace("`e", "").replace("`r", ChatColor.RED.toString()).replace("`R", ChatColor.DARK_RED.toString()).replace("`y", ChatColor.YELLOW.toString()).replace("`Y", ChatColor.GOLD.toString()).replace("`g", ChatColor.GREEN.toString()).replace("`G", ChatColor.DARK_GREEN.toString()).replace("`a", ChatColor.AQUA.toString()).replace("`A", ChatColor.DARK_AQUA.toString()).replace("`b", ChatColor.BLUE.toString()).replace("`B", ChatColor.DARK_BLUE.toString()).replace("`p", ChatColor.LIGHT_PURPLE.toString()).replace("`P", ChatColor.DARK_PURPLE.toString()).replace("`k", ChatColor.BLACK.toString()).replace("`s", ChatColor.GRAY.toString()).replace("`S", ChatColor.DARK_GRAY.toString()).replace("`w", ChatColor.WHITE.toString());
    }

    public static String upperCaseFirst(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    // -------------------------------------------- //
    // Color parsing
    // -------------------------------------------- //

    public static String implode(List<String> list, String glue) {
        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                ret.append(glue);
            }
            ret.append(list.get(i));
        }
        return ret.toString();
    }

    public static String repeat(String s, int times) {
        if (times <= 0) {
            return "";
        } else {
            return s + repeat(s, times - 1);
        }
    }

    public static String getMaterialName(Material material) {
        return material.toString().replace('_', ' ').toLowerCase();
    }

    // -------------------------------------------- //
    // Standard utils like UCFirst, implode and repeat.
    // -------------------------------------------- //

    public static String getMaterialName(int materialId) {
        return getMaterialName(Material.getMaterial(materialId));
    }

    public static String getBestStartWithCI(Collection<String> candidates, String start) {
        String ret = null;
        int best = 0;

        start = start.toLowerCase();
        int minlength = start.length();
        for (String candidate : candidates) {
            if (candidate.length() < minlength) {
                continue;
            }
            if (!candidate.toLowerCase().startsWith(start)) {
                continue;
            }

            // The closer to zero the better
            int lendiff = candidate.length() - minlength;
            if (lendiff == 0) {
                return candidate;
            }
            if (lendiff < best || best == 0) {
                best = lendiff;
                ret = candidate;
            }
        }
        return ret;
    }

    // -------------------------------------------- //
    // Material name tools
    // -------------------------------------------- //
    public static String parse(String str, Object... args) {
        return String.format(parse(str), args);
    }

    public static String parse(String str) {
        return parseColor(str);
    }

    // -------------------------------------------- //
    // Messaging helpers
    // -------------------------------------------- //
    public static void msgAll(String message) {
        for (HPlayer player : HPlayers.getInstance().getAllOnlinePlayers()) {
            player.msg(message);
        }
    }

    public static void msgAll(BaseComponent... components) {
        for (HPlayer player : HPlayers.getInstance().getAllOnlinePlayers()) {
            player.msg(components);
        }
    }

}
