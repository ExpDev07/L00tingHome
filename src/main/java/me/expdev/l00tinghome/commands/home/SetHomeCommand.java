package me.expdev.l00tinghome.commands.home;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.LazyLocation;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Project created by ExpDev
 */
public class SetHomeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can have homes :(.");
            return true;
        }

        // This is me!
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);

        // Get my current location
        LazyLocation location = me.getLocation();

        // Was no identifier specified? Set/modify default home
        if (args.length < 1) {
            // Take the player home, already know the player has a home
            me.setHome(location);
            me.msg("&aYou set your default home!");
            return true;
        }

        // Specified id for home
        String id = args[0].trim();

        // Set the home tied to the specified id
        me.setHome(id, location);

        me.msg("&aYou have set a new home (&2%1$s&a)!", id);
        return true;
    }
}