package me.expdev.l00tinghome.commands.home;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Project created by ExpDev
 */
public class HomeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can have homes :(.");
            return true;
        }

        // This is me!
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);
        if (!me.hasHome()) {
            me.msg("&cYou do not have a home set. Use /sethome <id> to settle down!");
            return true;
        }

        // Was no identifier specified? Timers to their one home
        if (args.length < 1) {
            // Take the player home, already know the player has a home
            me.goHome();
            me.msg("&aTeleported to your default home!");
            return true;
        }

        // Specified home
        String id = args[0].trim();
        if (me.goHome(id)) {
            me.msg("&aTeleported to your home (&2%1$s&a)!", id);
        } else {
            me.msg("&cYour home could not be found (&4%1$s&c). Use /sethome <id> to settle down!", id);
            me.sendMessage("&cYour homes are: &4" + StringUtils.join(me.getHomes().keySet(), ", ") + "&c.");
        }

        return true;
    }
}
