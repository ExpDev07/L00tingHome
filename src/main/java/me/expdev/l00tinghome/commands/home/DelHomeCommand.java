package me.expdev.l00tinghome.commands.home;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Project created by ExpDev
 */
public class DelHomeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can have homes :(.");
            return true;
        }

        // This is me!
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);

        // Do they have any homes?
        if (!me.hasHome()) {
            me.msg("&cYou do not have any homes to delete!");
            return true;
        }

        // Was no identifier specified? Set/modify default home
        if (args.length < 1) {
            // Take the player home, already know the player has a home
            me.removeHome();
            me.msg("&aYou removed your default home!");
            return true;
        }

        // Specified id for home
        String id = args[0].trim();

        // Set the home tied to the specified id
        me.removeHome(id);

        me.msg("&aYou have removed your home (&2%1$s&a)!", id);
        return true;
    }
}