package me.expdev.l00tinghome.commands.tp;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.TPRequest;
import me.expdev.l00tinghome.Timers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Project created by ExpDev
 */
public class TpaCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can use teleport :(.");
            return true;
        }

        // This is me!
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);

        // Was no target specified?
        if (args.length < 1) {
            me.msg("&cYou must specify a player to teleport to!");
            return true;
        }

        // Specified target
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            me.msg("&cThat player (&4%1$s&c) could not be found.", args[0]);
            return true;
        }

        // Specified target as HPlayer
        HPlayer they = HPlayers.getInstance().getByPlayer(target);

        // Time in seconds for request to expire
        int timeout = Timers.TP_TIMEOUT;

        // Set their tp request to a new one
        they.setTPRequest(new TPRequest(they, me, timeout));

        // Notify both parties
        me.msg("&aYou have requested to teleport to &2%1$s!", they.getName());

        they.msg("&aYou have recieved a teleport request from &2%1$s&a.", me.getName());
        they.msg("&aType \"&2/tpaccept&a\" to accept! It will expire in &2%1$s seconds&a.", String.valueOf(timeout));

        return true;
    }
}