package me.expdev.l00tinghome.commands.tp;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.TPRequest;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Project created by ExpDev
 */
public class TpAcceptCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can accept teleports :(.");
            return true;
        }

        // This is me!
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);

        // Do I have any pending requests?
        // Will also check if request is valid
        if (!me.hasTPRequest()) {
            me.msg("&cYou do not have any requests at the moment!");
            return true;
        }

        // The request
        TPRequest request = me.getTPRequest();

        // Has it expired?
        if (request.hasExpired()) {
            me.msg("&cYour most recent request has expired.");
            return true;
        }

        // Were we able to accept it?
        if (request.accept()) {
            // Player that requested the teleportation
            HPlayer requester = request.getRequester();

            // Notify both parties
            me.msg("&aYou accepted the teleport request from &2%1$s&a!", requester.getName());
            requester.msg("&2%1$s &ahas accepted your teleport request!", me.getName());
        } else {
            me.msg("&cRequest could not be accepted. Maybe the requester has logged off?");
        }

        return true;
    }
}