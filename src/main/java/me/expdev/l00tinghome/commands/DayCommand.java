package me.expdev.l00tinghome.commands;

import me.expdev.l00tinghome.DayVote;
import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.Timers;
import me.expdev.l00tinghome.util.TextUtil;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Project created by ExpDev
 */
public class DayCommand implements CommandExecutor {

    // World name -> vote
    private Map<String, DayVote> votes = new HashMap<String, DayVote>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, only players can vote on a day :(.");
            return true;
        }

        // This is me
        HPlayer me = HPlayers.getInstance().getByPlayer((Player) sender);

        // World I am in
        World world = Bukkit.getWorld(me.getLocation().getWorldName());
        if (world == null) {
            me.msg("&cWorld was null, something is wrong!");
            return true;
        }

        // Check if it is night before doing anything
        if (!DayVote.isNight(world)) {
            me.msg("&cYou cannot vote for daylight when it's already day. Wait until &413:00 &cbefore voting!");
            return true;
        }

        // Used for component builders
        String jsonStartVote = "[\"\",{\"text\":\"" + me.getName() + "\",\"color\":\"dark_green\"},{\"text\":\" has started a vote for daylight, type \\\"\",\"color\":\"green\"},{\"text\":\"/day\",\"color\":\"dark_green\"},{\"text\":\"\\\" or click \",\"color\":\"green\"},{\"text\":\"HERE\",\"color\":\"dark_green\",\"bold\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/day\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to vote!\",\"color\":\"gray\"}]}}},{\"text\":\" to vote!\",\"color\":\"green\",\"bold\":false}]";
        String jsonVote = "[\"\",{\"text\":\"" + me.getName() + "\",\"color\":\"dark_green\"},{\"text\":\" has voted for day, type \\\"\",\"color\":\"green\"},{\"text\":\"/day\",\"color\":\"dark_green\"},{\"text\":\"\\\" or click \",\"color\":\"green\"},{\"text\":\"HERE\",\"color\":\"dark_green\",\"bold\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/day\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"Click to vote!\",\"color\":\"gray\"}]}}},{\"text\":\" to vote!\",\"color\":\"green\",\"bold\":false}]";

        // The vote on day for my world
        DayVote vote = this.getVote(world.getName());

        // Do we already have a vote
        if (vote != null) {
            // Check if it is night
            if (vote.isNight()) {
                // Make sure it has not expired
                if (!vote.hasExpired()) {
                    // Have I already voted?
                    if (vote.hasVoted(me)) {
                        me.msg("&cHah, nice try! You cannot vote twice.");
                        return true;
                    }

                    // Add me as a voter
                    vote.addVote(me);



                    // Broadcast my vote!
                    TextUtil.msgAll(ComponentSerializer.parse(jsonVote));
                    TextUtil.msgAll("&aCurrent votes: &2" + (vote.getCurrentVotes()) + "/" + (vote.getNeededVotes()) + ".");
                    TextUtil.msgAll("&aExpires in: &2" + (vote.getExpiration() / 1000) + " seconds&a.");

                    // Approve, will check if vote is valid to go through
                    if (vote.approve()) {
                        TextUtil.msgAll("&aVote for day was successful. Rise, sun!");
                    } else {
                        me.msg("&cOops, something went wrong with your vote. Wait for this one to expire!");
                    }

                    return true;
                }
            }

        }

        // Needed votes for vote to go through, 60% of world population
        int neededVotes = (int) (world.getPlayers().size() * 0.60);

        // Time in seconds for vote to expire
        int timeout = Timers.DAY_VOTE_TIMEOUT;

        // It has expired and is null, make a new one!
        // Will add me as a voter
        DayVote newVote = new DayVote(me, world, neededVotes, timeout);

        // Set vote
        this.setVote(world.getName(), newVote);

        // Broadcast my startup!
        TextUtil.msgAll(ComponentSerializer.parse(jsonStartVote));
        TextUtil.msgAll("&aCurrent votes: &2" + (newVote.getCurrentVotes()) + "/" + (newVote.getNeededVotes()) + ".");
        TextUtil.msgAll("&aExpires in: &2" + (newVote.getExpiration() / 1000) + " seconds&a.");

        // Approve, will check if vote is valid to go through
        if (newVote.approve()) {
            TextUtil.msgAll("&aVote for day was successful. Rise, sun!");
        } else {
            me.msg("&cOops, something went wrong with starting your vote!");
        }

        return true;
    }

    /**
     * Get a vote for a specific world by the world name
     *
     * @param worldName World to get for
     * @return Vote for the world
     */
    private DayVote getVote(String worldName) {
        return votes.get(worldName);
    }

    /**
     * Set a vote for a specific world by the world name
     *
     * @param worldName World to set for
     * @param vote Vote to set
     */
    private void setVote(String worldName, DayVote vote) {
        votes.put(worldName, vote);
    }

}
