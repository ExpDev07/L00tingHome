package me.expdev.l00tinghome;

/**
 * Project created by ExpDev
 */
public class Timers {

    // Timeout in seconds for teleport request
    public static final int TP_TIMEOUT = 7;

    // Timeout in seconds for a day vote to expire
    public static final int DAY_VOTE_TIMEOUT = 30;

}
