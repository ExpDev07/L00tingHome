package me.expdev.l00tinghome.tasks;

import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.HomePlugin;

/**
 * Project created by ExpDev
 */


public class SaveTask implements Runnable {

    private static boolean running = false;

    private HomePlugin p;

    public SaveTask(HomePlugin p) {
        this.p = p;
    }

    public void run() {
        running = true;

        HPlayers.getInstance().forceSave();

        running = false;
    }

    public static boolean isRunning() {
        return running;
    }
}