package me.expdev.l00tinghome;

/**
 * Simple teleportation request
 */
public class TPRequest {

    // Player that recieved request
    private HPlayer player;
    // Player that sent the request
    private HPlayer requester;

    // Timeout time (expiration stamp)
    private long timeout;

    // Accepted state for request
    private boolean accepted;

    /**
     * Constructs a teleportation request
     *
     * @param player Player which recieved request
     * @param requester Player which sent request
     * @param timeoutSeconds Seconds for timeout to occur
     */
    public TPRequest(HPlayer player, HPlayer requester, int timeoutSeconds) {
        this.player = player;
        this.requester = requester;
        this.timeout = (timeoutSeconds * 1000L) + System.currentTimeMillis();
    }

    /**
     * Checks if the request is valid:
     * No counterparts are null nor offline, not already accepted, etc...
     *
     * @return True if valid
     */
    public boolean isValid() {
        return player != null
                && requester != null
                && player.isOnline()
                && requester.isOnline()
                && !isAccepted();
    }

    /**
     * Checks if the request has expired
     *
     * @return True if expired
     */
    public boolean hasExpired() {
        // Check if the current time is over the timeout expiration stamp
        return System.currentTimeMillis() > timeout;
    }

    /**
     * Teleports requester to player immediately
     *
     * @return True if request was accepted
     */
    public boolean accept() {
        // Check if the request is invalidated
        if (!isValid() || hasExpired()) return false;

        // Teleports... swooooosh
        requester.goTo(player.getLocation());

        // Mark accepted
        this.accepted = true;
        return true;
    }

    public HPlayer getPlayer() {
        return player;
    }

    public HPlayer getRequester() {
        return requester;
    }

    /**
     * @return True if request was accepted
     */
    public boolean isAccepted() {
        return accepted;
    }

    /**
     * Gets the milliseconds left for request to expire
     *
     * @return -1 if expired, otherwise ms for expiration
     */
    public long getExpiration() {
        if (this.hasExpired()) return -1L;

        return timeout - System.currentTimeMillis();
    }

}
