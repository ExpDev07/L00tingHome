package me.expdev.l00tinghome;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

/**
 * Interface for the HPlayer
 */
public interface HPlayer {

    /**
     * Called when player logs in
     */
    void login();

    /**
     * Called when player logs out
     */
    void logout();

    /**
     * Remove player from cache
     */
    void remove();

    /**
     * @return Id of player (usually the uuid)
     */
    String getId();

    /**
     * @param id Identifier for player
     */
    void setId(String id);

    /**
     * Name of player. Updated on login/logout and intervals
     *
     * @return Saved name of player
     */
    String getName();

    /**
     * @param newName Name for player
     */
    void setName(String newName);

    /**
     * @return Bukkit's wrapper for player
     */
    Player getPlayer();

    /**
     * @return Stamp when player last logged in
     */
    long getLastLoginTime();

    /**
     * @param stamp Stamp to represent when player last logged in
     */
    void setLastLoginTime(long stamp);

    /**
     * @return Time player has been online for
     */
    long getOnlineTime();

    /**
     * Checks if {@link #getPlayer()} does not return null
     *
     * @return True if player is online
     */
    boolean isOnline();

    /**
     * Sends player a simple message
     *
     * @param message Message to send
     */
    void sendMessage(String message);

    /**
     * Sends player a list of messages
     *
     * @param messages Messages to send
     */
    void sendMessage(List<String> messages);

    /**
     * Sends player a message with placeholders
     *
     * @param str Message to send
     * @param args Placeholders
     */
    void msg(String str, Object... args);

    /**
     * Sends player BaseComponents (part of the Bungee API)
     *
     * @param components Components to send
     */
    void msg(BaseComponent... components);

    /**
     * Checks if player has a certain permission
     *
     * @param permission Permission to check for
     * @return True if player has this permission
     */
    boolean hasPermission(String permission);

    /**
     * @return Player's current location as a LazyLocation
     */
    LazyLocation getLocation();

    /**
     * @return Player's homes
     */
    Map<String, LazyLocation> getHomes();

    /**
     * @param id Identifier for home
     * @return Location to player's home
     */
    LazyLocation getHome(String id);

    /**
     * @return Player's default home
     */
    LazyLocation getHome();

    /**
     * Removes a home
     *
     * @param id Identifier for home
     * @return True if home was removed
     */
    boolean removeHome(String id);

    /**
     * Removes default home
     *
     * @return True if home was removed
     */
    boolean removeHome();

    /**
     * @return True if player has a home
     */
    boolean hasHome();

    /**
     * Timers player to a location
     *
     * @param location Location to Timers to
     * @return True if teleportation went through
     */
    boolean goTo(LazyLocation location);

    /**
     * Go (Timers) to a home
     *
     * @param id Identifier for home
     * @return True if teleportation went through
     */
    boolean goHome(String id);

    /**
     * Go (Timers) to default home
     *
     * @return True if teleportation went through
     */
    boolean goHome();

    /**
     * Adds a new home
     *
     * @param id Identifier for home
     * @param location Location for home
     */
    void setHome(String id, LazyLocation location);

    /**
     * Sets the default home
     *
     * @param location Location for home
     */
    void setHome(LazyLocation location);

    TPRequest getTPRequest();

    void setTPRequest(TPRequest request);

    boolean hasTPRequest();

}
