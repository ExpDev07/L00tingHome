package me.expdev.l00tinghome;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.expdev.l00tinghome.commands.DayCommand;
import me.expdev.l00tinghome.commands.home.DelHomeCommand;
import me.expdev.l00tinghome.commands.home.HomeCommand;
import me.expdev.l00tinghome.commands.home.SetHomeCommand;
import me.expdev.l00tinghome.commands.tp.TpAcceptCommand;
import me.expdev.l00tinghome.commands.tp.TpaCommand;
import me.expdev.l00tinghome.listeners.PlayerListener;
import me.expdev.l00tinghome.listeners.SpreadListener;
import me.expdev.l00tinghome.tasks.SaveTask;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.lang.reflect.Modifier;

/**
 * Project created by ExpDev
 */
public class HomePlugin extends JavaPlugin {

    private static HomePlugin i = null;

    public static HomePlugin getInstance() {
        return i;
    }

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().
            disableHtmlEscaping().
            excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE).
            create();

    // Determines whether plugin load was successful
    private boolean loadSuccessful;

    // Tasks
    private Integer saveTask;

    @Override
    public void onLoad() {
        // Used to determine if it was a successful plugin enable
        this.loadSuccessful = false;

        // Singleton instance set
        i = this;

        // Ensure base folder exists before doing anything!
        getDataFolder().mkdirs();

        // Also make sure there is a "players" folder
        File f = new File(getDataFolder(), "players");
        f.mkdirs();

        // Save default config
        saveDefaultConfig();
    }

    @Override
    public void onEnable() {
        // Used to determine how long the enable process takes
        long start = System.currentTimeMillis();

        // Registering commands and listeners
        registerCommands();
        registerListeners();

        // Setting up tasks
        if (saveTask == null) {
            // Every 15 minutes
            long ticks = 20L * 60L * 15L;
            this.saveTask = Bukkit.getServer().getScheduler()
                    .scheduleSyncRepeatingTask(this, new SaveTask(this), ticks, ticks);
        }

        this.loadSuccessful = true;
        long finish = System.currentTimeMillis();
        getLogger().info("Plugin successfully loaded. That took " + (finish - start) + "ms.");
    }

    @Override
    public void onDisable() {
        // Make sure to shut all tasks down
        if (saveTask != null) {
            this.getServer().getScheduler().cancelTask(saveTask);
            saveTask = null;
        }

        // Only save data if plugin actually loaded successfully
        if (loadSuccessful) {
            getLogger().info("Plugin successfully loaded: saving everything synchronously. Please don't turn off!");

            // Saving data, sync
            HPlayers.getInstance().forceSave(true);

            getLogger().info("-> Done saving.");
        } else {
            getLogger().warning("Plugin did not load successfully. Shutting down without saving.");
        }
    }

    /**
     * Registers all of the plugin's commands
     */
    private void registerCommands() {
        getCommand("home").setExecutor(new HomeCommand());
        getCommand("sethome").setExecutor(new SetHomeCommand());
        getCommand("delhome").setExecutor(new DelHomeCommand());

        getCommand("tpa").setExecutor(new TpaCommand());
        getCommand("tpaccept").setExecutor(new TpAcceptCommand());

        getCommand("day").setExecutor(new DayCommand());
    }

    /**
     * Registers all of the plugin's listeners
     */
    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new SpreadListener(), this);
    }

}
