package me.expdev.l00tinghome.persist;

import com.google.common.base.Strings;
import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.Homes;
import me.expdev.l00tinghome.LazyLocation;
import me.expdev.l00tinghome.TPRequest;
import me.expdev.l00tinghome.util.TextUtil;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Project created by ExpDev
 */
public abstract class MemoryHPlayer implements HPlayer {

    // Account
    private String id;
    private String name;

    // Statistics
    private long lastLoginTime;

    // Homes
    private Map<String, LazyLocation> homes = new HashMap<String, LazyLocation>();

    // TP Requests
    private transient TPRequest tpRequest;

    /*
    Constructors
    */
    public MemoryHPlayer(String id) {
        this.id = id;
    }

    // GSON constructor
    public MemoryHPlayer() {}

    @Override
    public void login() {
        this.name = this.getPlayer().getName();
    }

    @Override
    public void logout() {
        this.name = this.getPlayer().getName();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Player getPlayer() {
        return Bukkit.getPlayer(UUID.fromString(this.getId()));
    }

    @Override
    public boolean isOnline() {
        return this.getPlayer() != null;
    }

    @Override
    public String getName() {
        return isOnline() ? getPlayer().getName() : name;
    }

    @Override
    public void setName(String newName) {
        this.name = newName;
    }

    @Override
    public long getLastLoginTime() {
        return lastLoginTime;
    }

    @Override
    public void setLastLoginTime(long stamp) {
        this.lastLoginTime = stamp;
    }

    @Override
    public long getOnlineTime() {
        if (!isOnline()) {
            return 0;
        }

        return System.currentTimeMillis() - getLastLoginTime();
    }

    /*
    Message sending helpers
    */
    @Override
    public void msg(String str, Object... args) {
        this.sendMessage(TextUtil.parse(str, args));
    }

    @Override
    public void msg(BaseComponent... components) {
        Player player = this.getPlayer();
        if (player == null) {
            return;
        }

        player.spigot().sendMessage(components);
    }

    @Override
    public void sendMessage(String msg) {
        Player player = this.getPlayer();
        if (player == null) {
            return;
        }

        if (msg.contains("{null}")) {
            return; // user wants this message to not send
        }
        if (msg.contains("/n/")) {
            for (String s : msg.split("/n/")) {
                sendMessage(s);
            }
            return;
        }
        player.sendMessage(TextUtil.parse(msg));
    }

    @Override
    public void sendMessage(List<String> msgs) {
        for (String msg : msgs) {
            this.sendMessage(msg);
        }
    }

    /*
    Permission helpers
     */
    @Override
    public boolean hasPermission(String permission) {
        // If no permission is specified, return true
        if (Strings.isNullOrEmpty(permission)) return true;

        Player player = this.getPlayer();

        // Player is not null and has the permission
        return player != null && player.hasPermission(permission);
    }

    @Override
    public LazyLocation getLocation() {
        Player player = this.getPlayer();
        if (player == null) {
            return null;
        }

        return new LazyLocation(player.getLocation());
    }

    @Override
    public Map<String, LazyLocation> getHomes() {
        return new HashMap<String, LazyLocation>(this.homes);
    }

    @Override
    public LazyLocation getHome(String id) {
        return homes.get(id.toLowerCase());
    }

    @Override
    public LazyLocation getHome() {
        // Check if they have a home with the default id first
        LazyLocation home = this.getHome(Homes.DEFAULT_HOME_ID);
        if (home != null) {
            return home;
        }

        Map<String, LazyLocation> homes = this.getHomes();
        if (!homes.isEmpty()) {
            // Return the first home
            return homes.entrySet().iterator().next().getValue();
        }

         return null;
    }

    @Override
    public boolean removeHome(String id) {
        if (!homes.containsKey(id)) {
            return false;
        }

        homes.remove(id);
        return true;
    }

    @Override
    public boolean removeHome() {
        // Check if they have a home with the default id first
        LazyLocation home = this.getHome(Homes.DEFAULT_HOME_ID);
        if (home != null) {
            return removeHome(Homes.DEFAULT_HOME_ID);
        }

        Map<String, LazyLocation> homes = this.getHomes();
        return !homes.isEmpty() && removeHome(homes.entrySet().iterator().next().getKey());

    }

    @Override
    public boolean hasHome() {
        return getHome() != null;
    }

    @Override
    public boolean goTo(LazyLocation location) {
        Player player = this.getPlayer();
        if (player == null || location == null) {
            return false;
        }

        player.teleport(location.getBukkitLocation());
        return true;
    }

    @Override
    public boolean goHome(String id) {
        LazyLocation home = this.getHome(id);

        return home != null && this.goTo(home);
    }

    @Override
    public boolean goHome() {
        return this.goTo(this.getHome());
    }

    @Override
    public void setHome(String id, LazyLocation location) {
        // Always lowercase
        homes.put(id.toLowerCase(), location);
    }

    @Override
    public void setHome(LazyLocation location) {
        setHome(Homes.DEFAULT_HOME_ID, location);
    }

    @Override
    public TPRequest getTPRequest() {
        return tpRequest;
    }

    @Override
    public void setTPRequest(TPRequest request) {
        this.tpRequest = request;
    }

    @Override
    public boolean hasTPRequest() {
        TPRequest request = this.getTPRequest();

        // If they have a request set and it is valid (both players online etc)
        return request != null && request.isValid();
    }

    public abstract boolean shouldBeSaved();

    public abstract void forceSave();

    public abstract void forceSave(boolean sync);

    public abstract HPlayer load();

}
