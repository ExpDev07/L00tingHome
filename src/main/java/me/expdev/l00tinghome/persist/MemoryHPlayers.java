package me.expdev.l00tinghome.persist;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Project created by ExpDev
 */


public abstract class MemoryHPlayers extends HPlayers {

    protected Map<String, HPlayer> players = new ConcurrentSkipListMap<String, HPlayer>(String.CASE_INSENSITIVE_ORDER);

    @Override
    public HPlayer getByPlayer(Player player) {
        return getById(player.getUniqueId().toString());
    }

    @Override
    public HPlayer getByOfflinePlayer(OfflinePlayer player) {
        return getById(player.getUniqueId().toString());
    }

    @Override
    public List<HPlayer> getAllPlayers() {
        return new ArrayList<HPlayer>(players.values());
    }

    @Override
    public List<HPlayer> getAllOnlinePlayers() {
        List<HPlayer> list = new ArrayList<HPlayer>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            list.add(this.getByPlayer(player));
        }
        return list;
    }

    @Override
    public HPlayer getById(String id) {
        // Put him in the map if absent
        return players.computeIfAbsent(id, k -> generateHPlayer(id));
    }

    @Override
    public void removePlayer(Player player) {
        removeId(player.getUniqueId().toString());
    }

    @Override
    public void removeId(String id) {
        players.remove(id);
    }

    public abstract HPlayer generateHPlayer(String id);

}
