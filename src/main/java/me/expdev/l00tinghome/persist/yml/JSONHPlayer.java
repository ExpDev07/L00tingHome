package me.expdev.l00tinghome.persist.yml;

import com.google.gson.Gson;
import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.HomePlugin;
import me.expdev.l00tinghome.persist.MemoryHPlayer;
import me.expdev.l00tinghome.util.DiscUtil;

import java.io.File;

/**
 * Project created by ExpDev
 */
public class JSONHPlayer extends MemoryHPlayer {

    private transient File file;
    private transient Gson gson;

    public JSONHPlayer(String id) {
        super(id);
    }

    // Empty GSON constructor
    public JSONHPlayer() {}

    @Override
    public void forceSave() {
        forceSave(true);
    }

    @Override
    public void forceSave(boolean sync) {
        this.loadFileAndGson();
        save(file, this, sync);
    }

    @Override
    public HPlayer load() {
        this.loadFileAndGson();

        JSONHPlayer player = this;
        if (file.exists()) {
            String content = DiscUtil.readCatch(file);
            if (content == null) {
                return null;
            }

            player = gson.fromJson(content, JSONHPlayer.class);
        }

        save(this.file, player, true); // Update the flatfile

        return player;
    }

    private boolean save(File target, JSONHPlayer data, boolean sync) {
        return DiscUtil.writeCatch(target, this.gson.toJson(data), sync);
    }

    private void loadFileAndGson() {
        if (file == null) {
            this.file = new File(HomePlugin.getInstance().getDataFolder() + "/players", this.getId() + ".json");
        }

        if (gson == null) {
            this.gson = HomePlugin.GSON;
        }
    }

    @Override
    public void remove() {
        // Remove them from the map
        HPlayers.getInstance().removeId(this.getId());
    }

    @Override
    public boolean shouldBeSaved() {
        return true;
    }

}
