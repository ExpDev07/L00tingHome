package me.expdev.l00tinghome.persist.yml;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.persist.MemoryHPlayers;

/**
 * Project created by ExpDev
 */


public class JSONHPlayers extends MemoryHPlayers {

    @Override
    public void clean() {

    }

    @Override
    public void load() {

    }

    @Override
    public void forceSave() {
        this.forceSave(true);
    }

    @Override
    public void forceSave(boolean sync) {
        for (HPlayer player : this.getAllPlayers()) {
            ((JSONHPlayer) player).forceSave(sync);
        }
    }

    @Override
    public HPlayer generateHPlayer(String id) {
        return new JSONHPlayer(id).load();
    }
}
