package me.expdev.l00tinghome.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockSpreadEvent;

/**
 * Project created by ExpDev
 */
public class SpreadListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onSpread(BlockSpreadEvent event) {
        // Source spreading
        Block source = event.getSource();

        // Do not allow fire to spread
        if (source.getType() == Material.FIRE) {
            event.setCancelled(true);
        }
    }
}
