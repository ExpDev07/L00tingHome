package me.expdev.l00tinghome.listeners;

import me.expdev.l00tinghome.HPlayer;
import me.expdev.l00tinghome.HPlayers;
import me.expdev.l00tinghome.persist.yml.JSONHPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Project created by ExpDev
 */


public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLogin(PlayerLoginEvent event) {
        // Player logging in
        Player player = event.getPlayer();

        // Making sure all online players have a HPlayer
        // Methods calling getPlayer() on this object will throw an exception
        HPlayer me = HPlayers.getInstance().getByPlayer(player);

        // Update the lastLoginTime for this player
        me.setLastLoginTime(System.currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onJoin(PlayerJoinEvent event) {
        // Player that joined
        Player player = event.getPlayer();

        // Here it is safe to call getPlayer() on this object
        HPlayer me = HPlayers.getInstance().getByPlayer(player);

        // Finally we can call login on them
        me.login();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLeave(PlayerQuitEvent event) {
        // Player that left
        Player player = event.getPlayer();

        // This is me
        HPlayer me = HPlayers.getInstance().getByPlayer(player);

        // Save data and remove them, not sync
        JSONHPlayer meJson = (JSONHPlayer) me;
        meJson.forceSave(false);
        meJson.remove();
    }

}