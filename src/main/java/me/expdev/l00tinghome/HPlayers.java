package me.expdev.l00tinghome;

import me.expdev.l00tinghome.persist.yml.JSONHPlayers;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Project created by ExpDev
 */
public abstract class HPlayers {

    private static HPlayers instance = getHPlayersImpl();

    public static HPlayers getInstance() {
        return instance;
    }

    private static HPlayers getHPlayersImpl() {
        return new JSONHPlayers();
    }

    /**
     * Cleans the cache
     */
    public abstract void clean();

    /**
     * See {@link #getById(String)}
     */
    public abstract HPlayer getByPlayer(Player player);

    /**
     * See {@link #getByPlayer(Player)}
     */
    public abstract HPlayer getByOfflinePlayer(OfflinePlayer player);

    /**
     * Gets the player wrapper from an Id
     *
     * @param id Id to get for
     * @return HPlayer wrapper for id
     */
    public abstract HPlayer getById(String id);

    /**
     * @return All players in cache
     */
    public abstract List<HPlayer> getAllPlayers();

    /**
     * Will load all online players that hasn't been loaded yet
     *
     * @return All online players
     */
    public abstract List<HPlayer> getAllOnlinePlayers();

    /**
     * See {@link #removeId(String)}
     */
    public abstract void removePlayer(Player player);

    /**
     * Removes a player by Id
     *
     * @param id Id to remove for
     */
    public abstract void removeId(String id);

    /**
     * Loads all players into cache
     */
    public abstract void load();

    /**
     * See {@link #forceSave(boolean)}, uses true as parameter
     */
    public abstract void forceSave();

    /**
     * Force saves all players in the cache
     *
     * @param sync False if operation should be done asynchronously
     */
    public abstract void forceSave(boolean sync);

}
