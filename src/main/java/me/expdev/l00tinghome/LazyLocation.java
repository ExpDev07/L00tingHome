package me.expdev.l00tinghome;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.Serializable;

/**
 * Project created by ExpDev
 */
public class LazyLocation implements Serializable {

    private transient static final long serialVersionUID = -6349901284620963314L;
    private transient Location location = null;

    private String worldName;
    private double x;
    private double y;
    private double z;
    private float pitch;
    private float yaw;

    /**
     * Creates a LazyLocation from a Bukkit location
     *
     * @param loc Location to create from
     */
    public LazyLocation(Location loc) {
        setBukkitLocation(loc);
    }

    /**
     * See {@link #LazyLocation(String, double, double, double)}, with yaw a and pitch
     *
     * @param yaw Yaw-angle
     * @param pitch Pitch-angle
     */
    public LazyLocation(String worldName, double x, double y, double z, float yaw, float pitch) {
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    /**
     * Manually creates a LazyLocation object
     *
     * @param worldName Name of the world
     * @param x X-coordinate
     * @param y Y-coordinate
     * @param z Z-coordinate
     */
    public LazyLocation(String worldName, double x, double y, double z) {
        this(worldName, x, y, z, 0, 0);
    }

    /**
     * @return LazyLocation to Bukkit location
     */
    public final Location getBukkitLocation() {
        // Make sure Location is initialized before returning it
        initLocation();
        return location;
    }

    /**
     * Sets a new Bukkit location
     *
     * @param loc Location to set
     */
    public void setBukkitLocation(Location loc) {
        this.location = loc;
        this.worldName = loc.getWorld().getName();
        this.x = loc.getX();
        this.y = loc.getY();
        this.z = loc.getZ();
        this.yaw = loc.getYaw();
        this.pitch = loc.getPitch();
    }


    /**
     * Initialize the Bukkit location from this LazyLocation
     */
    private void initLocation() {
        // if location is already initialized, simply return
        if (location != null) {
            return;
        }

        // get World; hopefully it's initialized at this point
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            throw new NullPointerException("World is null? Contact developer.");
        }

        // store the Location for future calls, and pass it on
        this.location = new Location(world, x, y, z, yaw, pitch);
    }

    /**
     * See {@link #compareTo(LazyLocation, LazyLocation)}, uses this as first parameter
     *
     * @param b Location to compare this object to
     */
    public boolean compareTo(LazyLocation b) {
        return compareTo(this, b);
    }

    //
    // Just basic setters and getters
    //
    public String getWorldName() {
        return worldName;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getPitch() {
        return pitch;
    }

    public double getYaw() {
        return yaw;
    }

    /**
     * Compares two locations to check if they match values (x, y, z)
     *
     * @param a Location to compare
     * @param b Location to compare with
     * @return true if a and b match
     */
    public static boolean compareTo(LazyLocation a, LazyLocation b) {
        // Checking if values are equal
        return (a.x == b.x)
                && (a.y == b.y)
                && (a.z == b.z)
                && (a.worldName.equalsIgnoreCase(b.worldName)
        );
    }
}