package me.expdev.l00tinghome;

import org.bukkit.World;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A vote for day in a world, somewhere
 */
public class DayVote {

    private HPlayer initiator;
    private World world;

    // Amount of votes needed for it to go through
    private int neededVotes;

    // Collection of current voters
    private Set<UUID> currentVoters;

    // Timeout time (expiration stamp)
    private long timeout;

    // Approval state of vote
    private boolean approved;

    public DayVote(HPlayer initiator, World world, int neededVotes, int timeoutSeconds) {
        this.initiator = initiator;
        this.world = world;
        this.neededVotes = neededVotes <= 0 ? 1 : neededVotes; // Always need 1 or more
        this.timeout = (timeoutSeconds * 1000L) + System.currentTimeMillis();

        this.currentVoters = new HashSet<UUID>();

        // Add initiator as a voter
        currentVoters.add(UUID.fromString(initiator.getId()));
    }

    /**
     * See {@link DayVote#isNight(World)}
     */
    public boolean isNight() {
        return isNight(world);
    }

    /**
     * Checks if the vote is valid:
     * Is night, is approved
     *
     * @return True if valid
     */
    public boolean isValid() {
        return isNight()
                && isApproved();
    }

    /**
     * Checks if the request has expired
     *
     * @return True if expired
     */
    public boolean hasExpired() {
        // Check if the current time is over the timeout expiration stamp
        return System.currentTimeMillis() > timeout;
    }

    /**
     * Gets the milliseconds left for vote to expire
     *
     * @return -1 if expired, otherwise ms for expiration
     */
    public long getExpiration() {
        if (this.hasExpired()) return -1L;

        return timeout - System.currentTimeMillis();
    }

    /**
     * Teleports requester to player immediately
     *
     * @return True if request was accepted
     */
    public boolean approve() {
        // Check if the vote is invalidated or expired
        if (!isValid() || hasExpired()) return false;

        // Make day!
        world.setTime(0);

        // Mark accepted
        this.approved = true;
        return true;
    }
    /**
     * @return True if vote was successful
     */
    public boolean isApproved() {
        return this.getCurrentVotes() >= neededVotes;
    }


    /**
     * Add a voter
     */
    public void addVote(UUID who) {
        currentVoters.add(who);
    }


    /**
     * See {@link #addVote(UUID)}
     */
    public void addVote(HPlayer who) {
        // String id to uuid
        addVote(UUID.fromString(who.getId()));
    }

    /**
     * @param who Player to check for
     * @return True if has voted
     */
    public boolean hasVoted(UUID who) {
        return currentVoters.contains(who);
    }

    /**
     * See {@link #hasVoted(UUID)}
     */
    public boolean hasVoted(HPlayer who) {
        return hasVoted(UUID.fromString(who.getId()));
    }

    public HPlayer getInitiator() {
        return initiator;
    }

    public World getWorld() {
        return world;
    }

    public int getNeededVotes() {
        return neededVotes;
    }

    public Set<UUID> getCurrentVoters() {
        return new HashSet<UUID>(this.currentVoters);
    }

    public int getCurrentVotes() {
        return currentVoters.size();
    }

    public void setNeededVotes(int neededVotes) {
        this.neededVotes = neededVotes;
    }

    /**
     * @return True if it is night in the world
     */
    public static boolean isNight(World world) {
        // < 13000 = day
        // > 13000 = night
        return world != null && world.getTime() > 13000L;
    }

}
